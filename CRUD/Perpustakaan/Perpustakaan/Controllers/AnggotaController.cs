﻿using Microsoft.AspNetCore.Mvc;
using Perpustakaan.datamodels;
using Perpustakaan.Services;
using Perpustakaan.viewmodels;

namespace med_id.Controllers
{
    public class AnggotaController : Controller
    {
        AnggotaService anggotaService;
        public AnggotaController(AnggotaService _anggotaService)
        {
            anggotaService = _anggotaService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            ViewBag.CurrentFilter = searchString;
            List<MAnggotum> data = await anggotaService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Fullname.ToLower().Contains(searchString.ToLower())).ToList();
            }
            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Fullname).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Fullname).ToList();
                    break;
            }

            return View(data);
        }
        public IActionResult Create()
        {
            MAnggotum data = new MAnggotum();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(MAnggotum dataParam)
        {
            // dataParam.CreatedBy = 1;
            VMResponse respon = await anggotaService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExsist(string name, int id)
        {
            bool isExist = await anggotaService.CheckName(name, id);
            return Json(isExist);
        }

        public async Task<IActionResult> Edit(int id)
        {
            MAnggotum data = await anggotaService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MAnggotum dataParam)
        {
            //dataParam.ModifiedBy = 1;
            VMResponse respon = await anggotaService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            MAnggotum data = await anggotaService.GetDataById(id);
            return PartialView(data);
        }

        /*      [HttpGet("HapusData")]*/
        public async Task<IActionResult> Delete(int id)
        {
            MAnggotum data = await anggotaService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id, int modifedby)
        {
            //int modifedby = 1;
            VMResponse respon = await anggotaService.Delete(id, modifedby);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index");
        }
    }
}
