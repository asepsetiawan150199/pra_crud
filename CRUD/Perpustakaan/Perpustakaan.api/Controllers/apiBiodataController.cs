﻿//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Perpustakaan.datamodels;
//using Perpustakaan.viewmodels;

//namespace Perpustakaan.api.Controllers
//{
//    [Route("Biodata")]
//    [ApiController]
//    public class apiBiodataController : ControllerBase
//    {
//        private readonly PerpustakaanContext db;
//        private VMResponse respon = new VMResponse();

//        public apiBiodataController(PerpustakaanContext _db)
//        {
//            db = _db;
//        }
//        [HttpGet("GetAllData")]
//        public List<MBiodatum> GetAllData()
//        {
//            List< MBiodatum > data = db.MBiodata.Where(a => a.IsDelete == false).ToList();
//            return data;
//        }

//        [HttpGet("GetDataById/{id}")]

//        public MBiodatum DataById(int id)
//        {
//            MBiodatum result = db.MBiodata.Where(a => a.Id == id).FirstOrDefault();
//            return result;
//        }

//        [HttpGet("CheckName/{name}/{id}")]

//        public bool CheckName(string name, int id)
//        {
//            MBiodatum data = new MBiodatum ();

//            if (id == 0) //untuk create
//            {
//                data = db.MBiodata.Where(a => a.Fullname == name && a.IsDelete == false).FirstOrDefault();
//            }
//            else //untuk edit
//            {
//                data = db.MBiodata.Where(a => a.Fullname == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
//            }

//            if (data != null)
//            {
//                return true;
//            }

//            return false;
//        }

//        [HttpPost("Save")]
//        public VMResponse Save(MBiodatum data)
//        {
//            data.CreatedOn = DateTime.Now;
//            data.IsDelete = false;

//            try
//            {
//                db.Add(data);
//                db.SaveChanges();

//                respon.Message = "Data success saves";

//            }
//            catch (Exception ex)
//            {
//                respon.Success = false;
//                respon.Message = "Faild saved" + ex.Message;

//            }
//            return respon;
//        }

//        [HttpPut("Edit")]

//        public VMResponse Edit(MBiodatum data)
//        {
//            MBiodatum dt = db.MBiodata.Where(a => a.Id == data.Id).FirstOrDefault();

//            if (dt != null)
//            {

//                dt.Id = data.Id;
//                dt.ModifiedBy = data.ModifiedBy;
//                dt.ModifiedOn = DateTime.Now;

//                try
//                {
//                    db.Update(dt);
//                    db.SaveChanges();

//                    respon.Message = "Data success saved";

//                }
//                catch (Exception ex)
//                {
//                    respon.Success = false;
//                    respon.Message = "Failed saved" + ex.Message;


//                }
//            }
//            else
//            {
//                respon.Success = false;
//                respon.Message = "Data Not Found";
//            }

//            return respon;
//        }

//        [HttpDelete("Delete/{id}/{modifedby}")]

//        public VMResponse Delete(int id, int modifedby)
//        {
//            MBiodatum dt = db.MBiodata.Where(a => a.Id == id).FirstOrDefault();
//            if (dt != null)
//            {
//                dt.IsDelete = true;
//                dt.ModifiedBy = modifedby;
//                dt.DeletedBy = modifedby;
//                dt.ModifiedOn = DateTime.Now;
//                dt.DeletedOn = DateTime.Now;

//                try
//                {
//                    db.Update(dt);
//                    db.SaveChanges();

//                    respon.Message = "Data success deleted";

//                }
//                catch (Exception ex)
//                {
//                    respon.Success = false;
//                    respon.Message = "Failed saved" + ex.Message;


//                }
//            }
//            else
//            {
//                respon.Success = false;
//                respon.Message = "Data Not Found";
//            }

//            return respon;


//        }
//    }
//}
