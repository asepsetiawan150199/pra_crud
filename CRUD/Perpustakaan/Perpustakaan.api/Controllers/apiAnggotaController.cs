﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Perpustakaan.datamodels;
using Perpustakaan.viewmodels;

namespace Perpustakaan.api.Controllers
{
    [Route("Anggota")]
    [ApiController]
    public class apiAnggotaController : ControllerBase
    {
        private readonly PerpustakaanContext db;
        private VMResponse respon = new VMResponse();

        public apiAnggotaController(PerpustakaanContext _db)
        {
            db = _db;
        }
        [HttpGet("GetAllData")]
        public List<MAnggotum> GetAllData()
        {
            List<MAnggotum> data = db.MAnggota.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]

        public MAnggotum DataById(int id)
        {
            MAnggotum result = db.MAnggota.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }

        [HttpGet("CheckName/{name}/{id}")]

        public bool CheckName(string name, int id)
        {
            MAnggotum data = new MAnggotum();

            if (id == 0) //untuk create
            {
                data = db.MAnggota.Where(a => a.Fullname == name && a.IsDelete == false).FirstOrDefault();
            }
            else //untuk edit
            {
                data = db.MAnggota.Where(a => a.Fullname == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(MAnggotum data)
        {
           
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saves";

            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Faild saved" + ex.Message;

            }
            return respon;
        }

        [HttpPut("Edit")]

        public VMResponse Edit(MAnggotum data)
        {
            MAnggotum dt = db.MAnggota.Where(a => a.Id == data.Id).FirstOrDefault();

            if (dt != null)
            {

                dt.Username = data.Username;
                dt.Kategori = data.Kategori;
                dt.Password = data.Password;
                dt.Email = data.Email;
                dt.Fullname = data.Fullname;
                dt.Alamat = data.Alamat;
                dt.Gender = data.Gender;
                dt.Phone = data.Phone; 
                if (data.ImagePath != null)
                {
                    dt.ImagePath = data.ImagePath;
                }
                else
                {
                    dt.ImagePath = "";
                }

                
                dt.ModifiedBy = data.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";

                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;


                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}/{modifedby}")]

        public VMResponse Delete(int id, int modifedby)
        {
            MAnggotum dt = db.MAnggota.Where(a => a.Id == id).FirstOrDefault();
            if (dt != null)
            {

                dt.IsDelete = true;
                dt.ModifiedBy = modifedby;
                dt.DeletedBy = modifedby;
                dt.ModifiedOn = DateTime.Now;
                dt.DeletedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";

                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;


                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;


        }
    }
}
