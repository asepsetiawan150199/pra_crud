﻿BLQ_19();
Console.ReadKey();

static void BLQ_19()
{
    Console.WriteLine("---PANGRAM---");

    string pangram = "abcdefghijklmnopqrstuvwxyz";

    Console.WriteLine("Masukan Kalimat = ");
    string kal = Console.ReadLine();

    int tampung = 0;
    for (int i = 0; i < kal.Length; i++)
    {
        for (int j = 0; j < pangram.Length; j++)
        {
            if (kal[i] == pangram[j])
            {
                tampung++;
                char a = pangram[j];
                pangram = pangram.Replace(a.ToString(), "");
            }
        }

        if (tampung == 26)
        {
            break;
        }

    }

    if (tampung == 26)
    {
        Console.WriteLine("Pangram");
    }
    else
    {
        Console.WriteLine("Not Pangram");
    }
}