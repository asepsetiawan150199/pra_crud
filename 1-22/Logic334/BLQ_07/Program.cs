﻿BLQ_07();
Console.ReadKey();

static void BLQ_07()
{
    Console.Write("masukan angka = ");

    double[] array = Array.ConvertAll(Console.ReadLine().Split(' ',','), Convert.ToDouble);

    double mean;
    double tam = 0;
    double a;
    double modus = array.Max();
    foreach (double item in array)
    {
        tam += item;
    }

    mean = tam / array.Length;

    Console.WriteLine("Nilai Mean = " + mean);

    for (int i = 0; i < array.Length; i++)
    {
        for (int j = i + 1; j < array.Length; j++)
        {
            if (array[j] < array[i]) 
            {
                a = array[i]; 
                array[i] = array[j];
                array[j] = a;
            }
        }
    }

    int tes = array.Length / 2;
    int cek = array.Length % 2;

    if (cek == 0)
    {
        Console.WriteLine("Nilai Median = " + array[tes] + " dan " + array[tes-1]);
    }
    else
    {
        Console.WriteLine("Nilai Median = " + array[tes]);
    }

    int banding = 0;
    for (int i = 0; i < array.Length; )
    {
        int nah = 1;
        for (int j = i + 1; j < array.Length; j++)
        {
            if (array[j] == array[i])
            {
                nah++;
            }
        }
        if (nah > banding)
        {
            modus = array[i];
            banding = nah;
        }
        else if (nah == banding && modus > array[i])
        {
            modus = array[i];
        }
       
        i += nah;

    }
    Console.WriteLine("Nilai Modus = " + modus);



}