﻿BLQ_03();
Console.ReadKey();

static void BLQ_03()
{
    double totalTarif = 0;
    Console.WriteLine("== Parsing Date Time ==");
    Console.Write("Waktu Masuk (dd/MM/yyyy|HH/mm/ss) : ");
    string dateString = Console.ReadLine();
    Console.Write("Waktu Keluar (dd/MM/yyyy) : ");
    string dateString1 = Console.ReadLine();

    DateTime date1 = DateTime.ParseExact(dateString, "d/M/yyyy|HH/mm/ss", null);
    DateTime date2 = DateTime.ParseExact(dateString1, "d/M/yyyy|H/m/s", null);
    //Console.WriteLine(date1);
    TimeSpan interval = date2 - date1;
    //Console.WriteLine("Lama Peminjaman : " + interval.Days);
    double inDays = interval.TotalSeconds;
    double jam = inDays / 3600;
    double hari = jam / 24;
    if (hari <= 1) 
    {
        if (jam <= 8)
        {
            totalTarif += Math.Ceiling(jam) * 1000;
        }
        else
        {
            totalTarif = 8000;
        }
    }
    else
    {
        double sisaJam = Math.Ceiling(jam - 24) * 1000;
        totalTarif = 15000 + sisaJam;
    }

    Console.WriteLine("Jumlah Tarif : " + totalTarif);
}